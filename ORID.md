**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. 了解了任务拆解三个核心点：易读，互不包含，可执行
2. 实践了拆解任务（找到大于10的cuteNumber）
3. 了解了git的基础使用
4. 了解了git的使用要求：频繁提交代码；不能提交未完成的代码；提交前需要进行测试
5. 了解了任务拆解的深入，拆解开发任务，上课拆解了“找到大于10的cuteNumber”任务，应该拆分为什么样的子任务，如何控制任务粒度
6. 学习了context map的思想，将一个任务划分为多个开发任务，通过context map将开发任务进行拆分。图中元素有：函数名，输入，输出
7. 实践了从绘制context map到执行开发任务的全流程

**R (Reflective): Please use one word to express your feelings about today's class.**

后知后觉

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

1. 在做作业时，最后输出的结果第2行与第3行的结果是颠倒的，后来neo提醒我HashMap是无序的，可以改成treeMap进行存储
2. 在实践代码作业时因为对java还是不太熟悉，老师上课提问的使用map存储数据回答不出来


**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

在今天老师给我们机会去拆分任务时，我习惯性的关注代码如何实现，从代码的视角去看代一个任务，导致在画context map时陷入误区，经过今天学习后，我明白了应该从任务的input output出发，定义方法，预估方法的实现复杂度，最后将代码复杂度分解到一定规模。