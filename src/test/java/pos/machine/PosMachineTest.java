package pos.machine;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PosMachineTest {

    @Test
    public void should_return_receipt() {
        PosMachine posMachine = new PosMachine();

        String expected = "***<store earning no money>Receipt***\n" +
                "Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)\n" +
                "Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)\n" +
                "Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)\n" +
                "----------------------\n" +
                "Total: 24 (yuan)\n" +
                "**********************";

        assertEquals(expected, posMachine.printReceipt(loadBarcodes()));
    }
    @Test
    public void test_loadAllItems() {
        PosMachine posMachine = new PosMachine();
        Item item = posMachine.findByBarcodes("ITEM000000");
        System.out.println(item.getName());
    }
    @Test
    public void test_getEachNum() {
        PosMachine posMachine = new PosMachine();
        Map<String,Integer> map = posMachine.getEachNum(loadBarcodes());
        System.out.println(map.get("ITEM000000"));
    }
    @Test
    public void test_decodeToItems(){
        PosMachine posMachine = new PosMachine();
        List<ReceiptItem> receiptItems = posMachine.decodeToItems(Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004"));
        System.out.println(receiptItems.get(1).getName());
    }
    @Test
    public void test_getReceiptLine(){
        PosMachine posMachine = new PosMachine();
        String s = posMachine.getReceiptLine(new ReceiptItem("233",12,10));
        System.out.println(s);

    }

    private static List<String> loadBarcodes() {
        return Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004");
    }
}
