package pos.machine;

import java.util.*;

//
public class PosMachine {
//    ***<store earning no money>Receipt***
//      Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)
//      Name: Sprite, Quantity: 2, Unit price: 3 (yuan), Subtotal: 6 (yuan)
//      Name: Battery, Quantity: 3, Unit price: 2 (yuan), Subtotal: 6 (yuan)
//----------------------
//Total: 24 (yuan)
//**********************
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        String receipt = "***<store earning no money>Receipt***\n";
        for (ReceiptItem receiptItem : receiptItems) {
            receipt += getReceiptLine(receiptItem)+"\n";
        }
        receipt += "----------------------\n";
        receipt += getTotalLine(receiptItems);
        return receipt;
    }
    public String getTotalLine(List<ReceiptItem> receiptItems) {
        int total = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            total += receiptItem.getSubTotal();
        }
        String line = "Total: " + total + " (yuan)";
        return line;
    }
//    获取receipt的数据行
    public String getReceiptLine(ReceiptItem receiptItem) {
        String line = "Name: " + receiptItem.getName() + ", Quantity: " + receiptItem.getQuantity() + ", Unit price: " + receiptItem.getUnitPrice() + " (yuan), Subtotal: " + receiptItem.getSubTotal() + " (yuan)";
        return line;
    }
//    将所有barcodes转换为ReceiptItemList
    public List<ReceiptItem> decodeToItems(List<String> barcodes){
        Map<String,Integer> map = getEachNum(barcodes);
        List<ReceiptItem> receiptItems = new ArrayList<>();

        map.forEach((key,value)->{
            Item item = findByBarcodes(key);
            ReceiptItem receiptItem = new ReceiptItem(item.getName(),value,item.getPrice());
            receiptItems.add(receiptItem);
        });

        return receiptItems;
    }

//    获取barcodes中各barcode的数目
    public Map<String,Integer> getEachNum(List<String> barcodes){
        Map<String,Integer> map = new TreeMap<>();
        for(String barcode:barcodes){
            if (!map.containsKey(barcode)){
                map.put(barcode,1);
                continue;
            }
            map.put(barcode,map.get(barcode) + 1);
        }
        return map;
    }

//    通过barcode获取Item
    public Item findByBarcodes(String barcode){
        List<Item> items = loadAllItems();
        Item item = null;
        for (Item i : items) {
            if (i.getBarcode()==barcode){
                item = i;
                break;
            }
        }
        return item;
    }
//    获取Items
    public List<Item> loadAllItems(){
        List<Item> items = ItemsLoader.loadAllItems();
        return items;
    }

}
